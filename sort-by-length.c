#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>

static bool d1 = false;
static bool d2 = false;

static unsigned int STD_START_SIZE = 2;
static unsigned int csize = 2;
static unsigned int cind = 0;

static bool rev = false;

typedef struct
{
    size_t len;
    char* content;
} line;

static line* lines;

int compare (const void * a, const void * b)
{
  line* lineA = (line *)a;
  line* lineB = (line *)b;

  return ( rev ? lineA->len - lineB->len : lineB ->len - lineA->len );
}

int resize()
{
    csize*=2;
    lines = realloc(lines, sizeof(line) * csize);
}

void add(line ln)
{
    if (d1) printf("size of line content in add: %ld\n", strlen(ln.content));
    if (cind + 1 == csize)
    {
        if (d1) printf("Resizing from %d to %d slots!\n", csize, csize*2);
        resize();
    }
    lines[cind] = ln;
    if (d2) printf("size of lines[cind] content after add: %ld\n", strlen(lines[cind].content));
    cind++;
}

void init()
{
    lines = malloc(STD_START_SIZE * sizeof (line));
}

int main(int argc, char *argv[])
{
    init();
    if (argc == 1 && strcmp(argv[0], "-r"))
        rev = true;

    char *word = malloc(1048 * sizeof (char));
    size_t size = 1048;
    long linesize;
    while ((linesize = getline(&word, &size, stdin)) > 0) {
        if (d1) printf("Found '%s'\n", word);
        line ln = {linesize, word};
        add(ln);
        word = malloc(1048 * sizeof (char));
    }

    if (d1) printf("Finished! Recorded %d lines!\n", cind);
    if (d1)
    {
        for (unsigned int i = 0; i < cind; i++)
        {
            line ln = lines[i];
            if (d1) printf("size of line content: %ld\n", strlen(ln.content));
            printf("%d: size: %ld, content: '%s'\n", i, ln.len, ln.content);
        }
    }

    if (d1) printf("Proceeding with qsort!\n");
    qsort (lines, cind, sizeof(line), compare);
    if (d2) printf("Sorting done!\n");
    for (unsigned int i = 0; i < cind; i++)
    {
        line ln = lines[i];
        if (d1) printf("size of line content: %ld\n", strlen(ln.content));
        if (d1)
            printf("%d: size: %ld, content: '%s'\n", i, ln.len, ln.content);
        else
            printf("%s", ln.content);
    }
    /*
    for (int i = 1; i < argc; i++)
    {
        char* word = argv[i];

    }
    */
    return 0;
}
